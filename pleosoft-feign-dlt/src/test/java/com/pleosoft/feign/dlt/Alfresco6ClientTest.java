package com.pleosoft.feign.dlt;

import org.alfresco.rest.api.model.AuditApp;
import org.alfresco.rest.api.model.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.pleosoft.feign.templating.AlfrescoEntry;
import com.pleosoft.feign.templating.AlfrescoList;
import com.pleosoft.feign.templating.TemplatingClientConfiguration;

//@TestPropertySource(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { TemplatingClientConfiguration.class })
@ImportAutoConfiguration({ FeignAutoConfiguration.class })
public class Alfresco6ClientTest {

	@Autowired
	private Alfresco6Client alfresco;

	@Before
	public void setup() {
	}

//	@Test
//	public void manualFeign() throws Exception {
//		Alfresco6Client client = Feign.builder().contract(new SpringMvcContract())
//				.requestInterceptor(new BasicAuthRequestInterceptor("admin", "admin"))
//				.target(Alfresco6Client.class, "https://vps1.pleosoft.com/alfresco/api/-default-/public/alfresco");
//
//		System.out.println();
//	}

	@Test
	public void alfrescoTest() throws Exception {

		AlfrescoEntry<Node> node = alfresco.getNode("a35fd8f3-3e46-4b6a-a395-d97c442d0f33");
		AlfrescoList<AuditApp> auditApplications = alfresco.getAuditApplications();

		System.out.println();
	}
}
