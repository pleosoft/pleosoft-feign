package com.pleosoft.feign.templating;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.hateoas.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

//@TestPropertySource(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { TemplatingClientConfiguration.class })
@ImportAutoConfiguration({ FeignAutoConfiguration.class })
public class TemplatingClientTest {

	@Autowired
	private TemplatingClient templating;

	@Before
	public void setup() {
	}

	// @Test
	// public void manualFeign() throws Exception {
	// Alfresco6Client client = Feign.builder().contract(new
	// SpringMvcContract())
	// .requestInterceptor(new BasicAuthRequestInterceptor("admin", "admin"))
	// .target(Alfresco6Client.class,
	// "https://vps1.pleosoft.com/alfresco/api/-default-/public/alfresco");
	//
	// System.out.println();
	// }

	@Test
	public void templatingTest() throws Exception {

		Resource<HashMap<String, Object>> actuators = templating.getActuators();

		System.out.println();
	}
}
