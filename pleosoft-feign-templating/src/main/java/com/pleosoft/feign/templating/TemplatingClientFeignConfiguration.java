package com.pleosoft.feign.templating;

import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.hal.Jackson2HalModule;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import feign.codec.Decoder;
import feign.jackson.JacksonDecoder;

public class TemplatingClientFeignConfiguration {

	@Bean
	public Decoder feignDecoder() {
		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.registerModule(new Jackson2HalModule());

		return new ResponseEntityDecoder(new JacksonDecoder(mapper));
	}

}
