package com.pleosoft.feign.templating;

import java.util.HashMap;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "templating", url="http://vps1.pleosoft.com:9292", configuration = TemplatingClientFeignConfiguration.class)
public interface TemplatingClient {

	@RequestMapping(method = RequestMethod.GET, value = "/actuator")
	Resource<HashMap<String, Object>> getActuators();
}
