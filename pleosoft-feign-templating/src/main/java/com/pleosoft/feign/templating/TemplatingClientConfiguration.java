package com.pleosoft.feign.templating;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackageClasses = { TemplatingClient.class })
public class TemplatingClientConfiguration {

}
