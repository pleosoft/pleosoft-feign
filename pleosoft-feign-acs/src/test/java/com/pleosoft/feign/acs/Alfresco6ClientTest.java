package com.pleosoft.feign.acs;

import java.util.ArrayList;
import java.util.List;

import org.alfresco.rest.api.model.Node;
import org.alfresco.rest.api.model.NodePermissions;
import org.alfresco.rest.api.model.NodePermissions.NodePermission;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;

import com.pleosoft.feign.acs.api.AlfrescoEntry;

@TestPropertySource(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { Alfresco6ClientConfiguration.class })
@ImportAutoConfiguration({ FeignAutoConfiguration.class })
public class Alfresco6ClientTest {

  @Autowired
  private Alfresco6Client alfresco;

  @Before
  public void setup() {}

  // @Test
  // public void manualFeign() throws Exception {
  // Alfresco6Client client = Feign.builder().contract(new SpringMvcContract())
  // .requestInterceptor(new BasicAuthRequestInterceptor("admin", "admin"))
  // .target(Alfresco6Client.class, "https://vps1.pleosoft.com/alfresco/api/-default-/public/alfresco");
  //
  // System.out.println();
  // }

  @Test
  public void alfrescoTest() throws Exception {

    // AlfrescoEntry<Node> node = alfresco.getNode("c89c32ce-8f7e-4d7f-88fd-3a3f17600f10");
    // AlfrescoList<AuditApp> auditApplications = alfresco.getAuditApplications();
    //
    // AlfrescoList<Node> search = alfresco.search(SearchQueryBuilder.build(QueryBuilder.build("10")));

    // MultipartFile file = new MockMultipartFile("filedata", "test.txt", "application/json", "test".getBytes());
//
    final ByteArrayResource file = new ByteArrayResource("test".getBytes()) {
      @Override
      public String getFilename() {
          return "test5.txt"; 
      }
    };
    
    final LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("filedata", file);
    body.add("overwrite", true);
    body.add("relativePath", "T/U/V");
    final AlfrescoEntry<Node> createFile = alfresco.createContent("-root-", body, new String [] {"path","permissions"});

    
    final NodePermissions permissions = new NodePermissions();//alfEntry.getEntry().getPermissions();
    permissions.setIsInheritanceEnabled(true);
    final List<NodePermission> localPermissions = new ArrayList<>();
    localPermissions.add(new NodePermission("admin", "Contributor", "ALLOWED"));
    permissions.setLocallySet(localPermissions);
    //node.setPermissions(new Node);
    
    

    final Node node = new Node();
    node.setNodeRef(createFile.getEntry().getNodeRef());
    node.setPermissions(permissions);
    alfresco.updateNode(createFile.getEntry().getNodeRef().getId(), node);
    System.out.println();
  }
}
