package com.pleosoft.feign.acs;

import org.alfresco.rest.api.search.model.SearchEntry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.gradecak.alfresco.mvc.rest.jackson.Jackson2NodeRefDeserializer;
import com.gradecak.alfresco.mvc.rest.jackson.Jackson2NodeRefSerializer;
import com.pleosoft.feign.acs.api.SearchEntryDeserializer;

import feign.RequestInterceptor;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;

public class Alfresco6ClientFeignConfiguration {
  
  @Value("${pleosoft.feign.alfresco6.authorizationValue:#{null}}") 
  private String defaultToken;

  @Bean
  public Retryer retryer() {
    return new Retryer.Default();
  }
  
  @Bean
  public ErrorDecoder errorDecoder() {
    return new ErrorDecoder.Default();
  }

  @Bean
  public Decoder feignDecoder() {
    return new ResponseEntityDecoder(new SpringDecoder(new ObjectFactory<HttpMessageConverters>() {

      @Override
      public HttpMessageConverters getObject() throws BeansException {
        // TODO Auto-generated method stub
        return httpMessageConverters();
      }
    }));
  }

  @Bean
  public Encoder feignEncoder() {
    return new SpringEncoder(new ObjectFactory<HttpMessageConverters>() {

      @Override
      public HttpMessageConverters getObject() throws BeansException {
        // TODO Auto-generated method stub
        return httpMessageConverters();
      }
    });
  }

  @Bean
  public HttpMessageConverters httpMessageConverters() {
    final HttpMessageConverter<?> jacksonConverter = new MappingJackson2HttpMessageConverter(objectMapper());

    return new HttpMessageConverters(jacksonConverter);
  }

  @Bean
  public ObjectMapper objectMapper() {
    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setSerializationInclusion(Include.NON_NULL);
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    final SimpleModule module = new SimpleModule();
    module.addDeserializer(NodeRef.class, new Jackson2NodeRefDeserializer());
    module.addDeserializer(SearchEntry.class, new SearchEntryDeserializer());
    module.addSerializer(NodeRef.class, new Jackson2NodeRefSerializer());

    objectMapper.registerModule(module);
    return objectMapper;
  }

  @Bean
  public RequestInterceptor headerAuthRequestInterceptor() {
    return new AuthorizationRequestInterceptor(AuthorizationHeaderEnum.X_FEIGN_ALF, defaultToken);
  }
}
