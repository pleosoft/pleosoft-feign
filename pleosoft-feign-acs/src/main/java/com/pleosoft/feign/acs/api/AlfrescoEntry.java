package com.pleosoft.feign.acs.api;

public class AlfrescoEntry<T> {

	private T entry;

	public T getEntry() {
		return entry;
	}

	public void setEntry(T entry) {
		this.entry = entry;
	}
}
