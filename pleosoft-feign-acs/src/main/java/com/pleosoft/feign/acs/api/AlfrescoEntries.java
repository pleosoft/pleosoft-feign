package com.pleosoft.feign.acs.api;

import java.util.List;

public class AlfrescoEntries<T> {

	private List<AlfrescoEntry<T>> entries;
	private AlfrescoPagination pagination;

	public List<AlfrescoEntry<T>> getEntries() {
		return entries;
	}
	
	public void setEntries(List<AlfrescoEntry<T>> entries) {
		this.entries = entries;
	}

	public AlfrescoPagination getPagination() {
		return pagination;
	}

	public void setPagination(AlfrescoPagination pagination) {
		this.pagination = pagination;
	}
}
