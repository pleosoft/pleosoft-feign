package com.pleosoft.feign.acs;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class AuthorizationRequestInterceptor implements RequestInterceptor {
  
  private final AuthorizationHeaderEnum header;
  private final String defaultToken;
  
  
  public AuthorizationRequestInterceptor(AuthorizationHeaderEnum header, String defaultToken) {
    this.header = header;
    this.defaultToken = defaultToken;
  }
  
  
  @Override
  public void apply(RequestTemplate template) {

    if(defaultToken != null) {
      template.header("Authorization", defaultToken);
    }
    
    final ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    if (requestAttributes == null) {
      return;
    }
    
    final HttpServletRequest request = requestAttributes.getRequest();
    if (request == null) {
      return;
    }
    
    final String header = request.getHeader(this.header.getValue());
    if (header == null) {
      return;
    }

    template.header("Authorization", header);
  }

}
