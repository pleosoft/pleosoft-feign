package com.pleosoft.feign.acs;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class AlfrescoRequestBody<K, V>  {
	
	final private LinkedMultiValueMap<K, V> body = new LinkedMultiValueMap<>();
	
	public AlfrescoRequestBody<K, V> add(K key, V value) {
		body.add(key, value);
		return this;
	}
	
	public MultiValueMap<K, V> build() {
//		body.add("overwrite", true);
//		body.add("filedata", file);
//		body.add("relativePath", "T/U/V");
	  return body;	
	}	
}
