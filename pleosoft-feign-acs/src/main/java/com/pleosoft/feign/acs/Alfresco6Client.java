package com.pleosoft.feign.acs;

import org.alfresco.rest.api.model.AuditApp;
import org.alfresco.rest.api.model.Node;
import org.alfresco.rest.api.search.model.SearchQuery;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.pleosoft.feign.acs.api.AlfrescoEntry;
import com.pleosoft.feign.acs.api.AlfrescoList;

@FeignClient(name = "alfresco6", url = "${pleosoft.feign.alfresco6.v1}", configuration = Alfresco6ClientFeignConfiguration.class)
public interface Alfresco6Client {

	@RequestMapping(method = RequestMethod.GET, value = "/api/-default-/public/alfresco/versions/1/audit-applications")
	AlfrescoList<AuditApp> getAuditApplications();

	@RequestMapping(method = RequestMethod.GET, value = "/api/-default-/public/alfresco/versions/1/nodes/{nodeId}")
	AlfrescoEntry<Node> getNode(@PathVariable("nodeId") String nodeId, @RequestParam("include") String[] include);

	/**
	 * See https://api-explorer.alfresco.com/api-explorer/#!/nodes/createNode for available values in body map 
	 * 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/api/-default-/public/alfresco/versions/1/nodes/{parentId}/children", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	AlfrescoEntry<Node> createContent(@PathVariable("parentId") String parentId,
	    @RequestBody LinkedMultiValueMap<String, Object> body, @RequestParam("include") String[] include);
	
	@RequestMapping(method = RequestMethod.GET, value = "/api/-default-/public/alfresco/versions/1/nodes/{nodeId}/content")
	byte[] getNodeContent(@PathVariable("nodeId") String nodeId);
	
	@RequestMapping(method = RequestMethod.PUT, value = "/api/-default-/public/alfresco/versions/1/nodes/{nodeId}/content", consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AlfrescoEntry<Node> updateNodeContent(@PathVariable("nodeId") String nodeId, @RequestBody byte[] contentBodyUpdate, @RequestParam("majorVersion") boolean majorVersion, @RequestParam("minorVersion") boolean minorVersion);
    
	/**
	 * See https://api-explorer.alfresco.com/api-explorer/#!/nodes/updateNode for details on update request
	 * 
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/api/-default-/public/alfresco/versions/1/nodes/{nodeId}")
    AlfrescoEntry<Node> updateNode(@PathVariable("nodeId") String nodeId, @RequestBody Node node);

	@RequestMapping(method = RequestMethod.GET, value = "/api/-default-/public/alfresco/versions/1/nodes/{parentId}/children")
	AlfrescoList<Node> getNodeChildren(@PathVariable("parentId") String parentId);
	   
	@RequestMapping(method = RequestMethod.POST, value = "api/-default-/public/search/versions/1/search")
	AlfrescoList<Node> search(@RequestBody SearchQuery search);
}
