package com.pleosoft.feign.acs.api;

import java.io.IOException;

import org.alfresco.rest.api.search.model.SearchEntry;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SearchEntryDeserializer extends JsonDeserializer<SearchEntry> {
	@Override
	public SearchEntry deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonNode node = jp.readValueAsTree();
        String score = node.has("score") ? node.get("score").asText() : null;
        String highlight = node.has("highlight") ? node.get("highlight").asText() : null;
        
        if(!StringUtils.hasText(score)){
        	if(!StringUtils.hasText(highlight)){
        		return new SearchEntry(null, null);
        	}        	
        	return new SearchEntry(null, null);
        }
	    
	    return new SearchEntry(Float.valueOf(score), null);
	}
}
