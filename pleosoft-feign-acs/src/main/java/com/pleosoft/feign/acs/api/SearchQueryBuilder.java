package com.pleosoft.feign.acs.api;

import org.alfresco.rest.api.search.model.Query;
import org.alfresco.rest.api.search.model.SearchQuery;

public class SearchQueryBuilder {

	public static SearchQuery build(final Query query) {
		return new SearchQuery(query, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null);
	}
}
