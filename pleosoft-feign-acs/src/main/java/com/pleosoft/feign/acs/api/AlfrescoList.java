package com.pleosoft.feign.acs.api;

public class AlfrescoList<T> {

	private AlfrescoEntries<T> list;
	
	public AlfrescoEntries<T> getList() {
		return list;
	}
	
	public void setList(AlfrescoEntries<T> list) {
		this.list = list;
	}
}
