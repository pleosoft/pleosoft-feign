package com.pleosoft.feign.acs;

public enum AuthorizationHeaderEnum {
  X_FEIGN_ALF("X-FEIGN-ALF"), X_FEIGN_ALF_ADMIN("X-FEIGN-ALF-ADMIN");

  private String value;

  private AuthorizationHeaderEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
