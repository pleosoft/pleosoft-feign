package com.pleosoft.feign.acs.api;

import org.alfresco.rest.api.search.model.Query;

public class QueryBuilder {

	public static Query build(final String term) {
		return new Query("afts", term, null);
	}
}
