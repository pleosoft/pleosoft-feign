package com.pleosoft.feign.acs;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackageClasses = { Alfresco6Client.class })
public class Alfresco6ClientConfiguration {

}
